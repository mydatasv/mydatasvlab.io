---
title: Project App Assay for COVID-19
date: 2020-05-01 10:00:00
---

{{% slide-in-img-right src="/assets/appassay-logo_150x150.png" %}}

Sari, Josep and Johannes, of the MyData Silicon Valley Hub have started
[Project App Assay for COVID-19](https://www.appassay.org/), which
analyzes the various apps being developed to fight back the pandemic.

Some of them are done quite nicely with respect to privacy and not creating
unnecessary personal surveillance. While other apps are either cavalier or
deceptive about it.

We are quite passionate about it, think it's worthwhile to analyze this, and
tell the world about it.

To find out how to contribute, go to
[www.appassay.org](https://www.appassay.org/).
