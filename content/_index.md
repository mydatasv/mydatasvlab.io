---
title: Welcome to the Hub
class: front
---

### Meet and discuss

Check out our [monthly meetings](/monthly-meetings/) and [events](/events/).
Typically every 2nd Thursday of the month, ~~in San Francisco~~ by video conference.

**Next meeting**: Thursday, January 14, 2021, at 11am pacific. Connection information will be
posted to the [Slack channel](https://mydataglobal.slack.com/messages/CEGG4282D) just
before start on the day of the event.

We will be:

* sharing news about the world of personal data (bring some!)
* debrief from the MyData Global conference
* plan for what we do in 2021 to assist the responsible actors in the personal data ecosystem.

Feel free to join, everybody is welcome.

### Stay in touch

**Slack** -- following the lead of the [MyData.org](https://mydata.org/) parent
organization, we use Slack. MyData Silicon Valley is at
[#hub-silicon-valley](https://mydataglobal.slack.com/messages/CEGG4282D), and
there's also the related
[#startups](https://mydataglobal.slack.com/messages/CKLN1SATZ) channel. Feel free to join us.

<div id="groupsio_embed_signup">
 <form action="https://groups.io/g/mydatasv-announce/signup?u=6321126547313652660" method="post" id="groupsio-embedded-subscribe-form" name="groupsio-embedded-subscribe-form" target="_blank">
  <div id="groupsio_embed_signup_scroll">
   <label for="email" id="templateformtitle"><b>Announcement e-mail list:</b></label>
   <input type="email" value="" name="email" class="email" id="email" placeholder="email address" required="">
   <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6321126547313652660" tabindex="-1" value=""></div>
   <input type="submit" value="Subscribe" name="subscribe" id="groupsio-embedded-subscribe" class="button">
  </div>
 </form>
</div>

