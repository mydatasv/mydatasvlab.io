---
title: Personal Data over Drinks
date: 2019-09-13 23:00:00
---

<img src="/events/personal-data-over-drinks-2019-09-13/personal-data-over-drinks.jpg" style="float: right; margin: 0 0 20px 20px; width: 260px; ">

**When:** Friday, September 13, 2019, 5-7pm.

**Where:** At [Tied House Cafe & Brewery, 954 Villa St, Mountain View, CA](https://www.openstreetmap.org/?mlat=37.79730&mlon=-122.40552#map=19/37.79730/-122.40552).

To RSVP, use:
[Eventbrite](https://www.eventbrite.com/e/my-data-silicon-valley-monthly-full-moon-mixer-tickets-71886908679),
or [Meetup](https://www.meetup.com/MyData-Silicon-Valley/events/264667104/).

This month, we invite you to join us to discuss personal data, privacy,
surveillance capitalism and related topics over drinks on the peninsula.

