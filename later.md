
{{% box %}}
### Next meeting

**When:** Thursday, April 9, 2020, 12:00noon to 1:30pm.

**Where:** online

For last-minute details, check the [#hub-silicon-valley](https://mydataglobal.slack.com/messages/CEGG4282D)
Slack channel.
{{% /box %}}





https://fevermap.net/
http://ncov.mohw.go.kr/selfcheck




Will Knight:
[The Value and Ethics of Using Phone Data to Monitor Covid-19](https://www.wired.com/story/value-ethics-using-phone-data-monitor-covid-19/).
Wired Magazine, March 18, 2020.

Sidney Fussell:
[How Surveillance Could Save Lives Amid a Public Health Crisis](https://www.wired.com/story/surveillance-save-lives-amid-public-health-crisis/).
Wired Magazine, March 21, 2020.

Matthew Guariglia and Adam Schwartz:
[Protecting Civil Liberties During a Public Health Crisis](https://www.eff.org/deeplinks/2020/03/protecting-civil-liberties-during-public-health-crisis)
EFF, March 10, 2020.

Wendy Davis:
[Government Plan To Fight COVID-19 Could Result In 'Wholesale Privacy Invasion,' Senator Warns](https://www.mediapost.com/publications/article/348766/)
MediaPost, March 19. 2020

Michael Geist:
[How Canada Should Ensure Cellphone Tracking to Counter the Spread of Coronavirus Does Not Become the New Normal](http://www.michaelgeist.ca/2020/03/how-canada-should-ensure-cellphone-tracking-to-counter-the-spread-of-coronavirus-does-not-become-the-new-normal/)
Blog, March 24, 2020





Event backlog
=============


Title tbd
 * Featured guest: Brittany Kaiser
 * October 2019 (tentative):
 * Organizer: Josep (to be confirmed)


Invitation-only dinner
 * October 2019 (tentative):


Cities, local governments and personal data
 * A variety of interesting personal data initiatives by cities and other local governments
   are going on world-wide. Have several of them present, focus on use cases, e.g.
   better health, better research etc.
 * Maybe January 2020


Startups and investors
 * A few MyData-related startups pitch
 * Perhaps a few investors interested in MyData present their views, too


Use case brainstorming
 * The upside of sharing personal data: what can we do then that we couldn't before?


